﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Course
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public Teacher TeacherCode { get; set; }

        public Category CategoryCode { get; set; }

        public double Price { get; set; }

        public bool Enable { get; set; }
    }
}
