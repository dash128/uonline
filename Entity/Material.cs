﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Material
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Link { get; set; }

        public Theme ThemeCode { get; set; }
    }
}
