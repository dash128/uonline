﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Theme
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public Unit UnitCode { get; set; }
    }
}
