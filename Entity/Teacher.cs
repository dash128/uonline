﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Teacher
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        public string Dni { get; set; }

        public string Email { get; set; }

        public Boolean Sex { get; set; }

        public string Phone { get; set; }
}
}
