﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;

namespace Data.implementation
{
    public class CourseRepository : ICourseRepository
    {
        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public List<Course> FindAll()
        {
            var courses = new List<Course>();

            try{
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["uonline"].ToString())){
                    con.Open();

                    var query = new SqlCommand("SELECT c.Id, c.Name, c.Description, t.Name teacherName, ca.Name categoryName, c.Price, c.Enable FROM Course c, Teacher t, Category ca WHERE c.CategoryId= ca.ID AND c.TeacherId= t.Id", con);

                    using (var dr = query.ExecuteReader()){

                        while (dr.Read()){

                            var course = new Course();
                            var teacher = new Teacher();
                            var category = new Category();

                            course.Id = Convert.ToInt32(dr["Id"]);
                            course.Name = dr["Name"].ToString();
                            course.Description = dr["Description"].ToString();
                            course.Price = Convert.ToDouble(dr["Price"]);
                            course.Enable = Convert.ToBoolean(dr["Enable"]);
                            category.Name = dr["categoryName"].ToString();
                            teacher.Name = dr["teacherName"].ToString();

                            course.CategoryCode = category;
                            course.TeacherCode = teacher;

                            courses.Add(course);
                        }
                    }
                }
            }
            catch (Exception ex){
                throw;
            }
            return courses;
        }

        public List<Course> FindByCategory(Category c)
        {
            throw new NotImplementedException();
        }

        public Course FindById(int? id)
        {
            Course course = null;

            try{
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["uonline"].ToString())){
                    con.Open();

                    var query = new SqlCommand("SELECT Id, Name, Description, Price FROM Course WHERE Id='" + id + "'", con);

                    using (var dr = query.ExecuteReader()){

                        while (dr.Read()){

                            course = new Course();
                            course.Id = Convert.ToInt32(dr["Id"]);
                            course.Name = dr["Name"].ToString();
                            course.Description = dr["Description"].ToString();
                            course.Price = Convert.ToDouble(dr["Price"].ToString());

                        }
                    }
                }
            }
            catch (Exception ex){
                throw;
            }
            return course;
        }

        public List<Course> FindByTeacher(Teacher t)
        {
            var courses = new List<Course>();

            try{
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["uonline"].ToString())){
                    con.Open();

                    var query = new SqlCommand("SELECT c.Id, c.Name, c.Description, ca.Name CategoryName, c.Price  FROM Course c INNER JOIN Category ca ON ca.Id = c.CategoryId WHERE c.TeacherId='"+t.Id+"'", con);

                    using (var dr = query.ExecuteReader()){

                        while (dr.Read()){

                            var course = new Course();
                            var category = new Category();

                            course.Id = Convert.ToInt32(dr["Id"]);
                            course.Name = dr["Name"].ToString();
                            course.Description = dr["Description"].ToString();
                            course.Price = Convert.ToDouble(dr["Price"]);
                            category.Name = dr["categoryName"].ToString();

                            course.CategoryCode = category;

                            courses.Add(course);
                        }
                    }
                }
            }
            catch (Exception ex){
                throw;
            }
            return courses;
        }

        public bool Insert(Course c)
        {
            bool rpta = false;

            try{
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["uonline"].ToString())){
                    con.Open();

                    var query = new SqlCommand("INSERT INTO Course (Name, Description, TeacherId, CategoryId , Price, Enable) VALUES (@Name, @Description, @TeacherId, @CategoryId, @Price, @Enable)", con);

                    query.Parameters.AddWithValue("@Name", c.Name);
                    query.Parameters.AddWithValue("@Description", c.Description);
                    query.Parameters.AddWithValue("@TeacherId", c.TeacherCode.Id);
                    query.Parameters.AddWithValue("@CategoryId", c.CategoryCode.Id);
                    query.Parameters.AddWithValue("@Price", c.Price);
                    query.Parameters.AddWithValue("@Enable", c.Enable);

                    query.ExecuteNonQuery();

                    rpta = true;
                }
            }
            catch (Exception ex){
                throw;
            }

            return rpta;
        }

        public bool Update(Course t)
        {
            throw new NotImplementedException();
        }
    }
}
