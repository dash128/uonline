﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;

namespace Data.implementation
{
    public class UnitRepository : IUnitRepository
    {
        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public List<Unit> FindAll()
        {
            throw new NotImplementedException();
        }

        public List<Unit> FindByCourse(Course c)
        {
            var units = new List<Unit>();

            try{
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["uonline"].ToString())){
                    con.Open();

                    var query = new SqlCommand("SELECT u.Id, u.Name  FROM Unit u WHERE u.CourseId='" + c.Id + "'", con);

                    using (var dr = query.ExecuteReader()){

                        while (dr.Read()){

                            var unit = new Unit();
                            

                            unit.Id = Convert.ToInt32(dr["Id"]);
                            unit.Name = dr["Name"].ToString();

                            units.Add(unit);
                        }
                    }
                }
            }
            catch (Exception ex){
                throw;
            }
            return units;
        }

        public Unit FindById(int? id)
        {
            throw new NotImplementedException();
        }

        public bool Insert(Unit u)
        {
            bool rpta = false;

            try{
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["uonline"].ToString())){
                    con.Open();

                    var query = new SqlCommand("INSERT INTO Unit (Name, CourseId) VALUES (@Name, @CourseId)", con);

                    query.Parameters.AddWithValue("@Name", u.Name);
                    query.Parameters.AddWithValue("@CourseId", 1);

                    query.ExecuteNonQuery();

                    rpta = true;
                }
            }catch (Exception ex){
                throw;
            }

            return rpta;
        }

        public bool Update(Unit t)
        {
            throw new NotImplementedException();
        }
    }
}
