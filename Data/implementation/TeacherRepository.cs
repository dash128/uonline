﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;

namespace Data.implementation
{
    public class TeacherRepository : ITeacherRepository
    {
        public List<Teacher> FindAll()
        {
            var teachers = new List<Teacher>();

            try {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["uonline"].ToString())) {
                    con.Open();

                    var query = new SqlCommand("SELECT Id, Name, LastName, Dni, Email, Sex, Phone FROM Teacher", con);

                    using (var dr = query.ExecuteReader()) {

                        while (dr.Read()) {

                            var teacher = new Teacher();
                            teacher.Id = Convert.ToInt32(dr["Id"]);
                            teacher.Name = dr["Name"].ToString();
                            teacher.LastName = dr["LastName"].ToString();
                            teacher.Dni = dr["Dni"].ToString();
                            teacher.Email = dr["Email"].ToString();
                            teacher.Sex = Convert.ToBoolean(dr["Sex"]);
                            teacher.Phone = dr["Phone"].ToString();

                            teachers.Add(teacher);
                        }
                    }
                }
            } catch (Exception ex) {
                throw;
            }
            return teachers;
        }

        public Teacher FindById(int? id)
        {
            Teacher teacher = null;

            try{
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["uonline"].ToString())){
                    con.Open();

                    var query = new SqlCommand("SELECT Id, Name, LastName, Dni, Email, Sex, Phone FROM Teacher WHERE Id='"+id+"'", con);

                    using (var dr = query.ExecuteReader()){

                        while (dr.Read()){

                            teacher = new Teacher();
                            teacher.Id = Convert.ToInt32(dr["Id"]);
                            teacher.Name = dr["Name"].ToString();
                            teacher.LastName = dr["LastName"].ToString();
                            teacher.Dni = dr["Dni"].ToString();
                            teacher.Email = dr["Email"].ToString();
                            teacher.Sex = Convert.ToBoolean(dr["Sex"]);
                            teacher.Phone = dr["Phone"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex){
                throw;
            }
            return teacher;
        }

        public bool Insert(Teacher t)
        {
            bool rpta = false;

            try {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["uonline"].ToString())) {
                    con.Open();

                    var query = new SqlCommand("INSERT INTO Teacher VALUES (@Dni, @Name, @LastName, @Email, @Sex, @Phone)", con);

                    query.Parameters.AddWithValue("@Name", t.Name);
                    query.Parameters.AddWithValue("@LastName", t.LastName);
                    query.Parameters.AddWithValue("@Dni", t.Dni);
                    query.Parameters.AddWithValue("@Email", t.Email);
                    query.Parameters.AddWithValue("@Sex", t.Sex);
                    query.Parameters.AddWithValue("@Phone", t.Phone);

                    query.ExecuteNonQuery();

                    rpta = true;
                }
            } catch (Exception ex) {
                throw;
            }

            return rpta;
        }

        public bool Update(Teacher t)
        {
            bool rpta = false;

            try{
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["uonline"].ToString())){
                    con.Open();

                    var query = new SqlCommand("UPDATE Teacher SET Name=@Name, LastName=@LastName, Dni=@Dni, Email=@Email, Sex=@Sex, Phone=@Phone WHERE Id=@Id", con);

                    query.Parameters.AddWithValue("@Id", t.Id);
                    query.Parameters.AddWithValue("@Name", t.Name);
                    query.Parameters.AddWithValue("@LastName", t.LastName);
                    query.Parameters.AddWithValue("@Dni", t.Dni);
                    query.Parameters.AddWithValue("@Email", t.Email);
                    query.Parameters.AddWithValue("@Sex", t.Sex);
                    query.Parameters.AddWithValue("@Phone", t.Phone);

                    query.ExecuteNonQuery();

                    rpta = true;
                }
            }
            catch (Exception ex){
                throw;
            }

            return rpta;
        }

        public bool Delete(int id)
        {
            bool rpta = false;

            try{
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["uonline"].ToString())){
                    con.Open();

                    var query = new SqlCommand("DELETE FROM Teacher WHERE Id='" + id + "'", con);
                    query.ExecuteNonQuery();

                    rpta = true;
                }
            }
            catch (Exception ex){
                throw;
            }

            return rpta;
        }
    }
}
