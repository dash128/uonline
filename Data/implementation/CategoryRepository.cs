﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;

namespace Data.implementation
{
    public class CategoryRepository : ICategoryRepository
    {
        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public List<Category> FindAll()
        {
            var categories = new List<Category>();

            try{
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["uonline"].ToString())){
                    con.Open();

                    var query = new SqlCommand("SELECT Id, Name FROM Category", con);

                    using (var dr = query.ExecuteReader()){

                        while (dr.Read()){

                            var category = new Category();
                            category.Id = Convert.ToInt32(dr["Id"]);
                            category.Name = dr["Name"].ToString();
                            
                            categories.Add(category);
                        }
                    }
                }
            }
            catch (Exception ex){
                throw;
            }
            return categories;
        }

        public Category FindById(int? id)
        {
            Category category = null;

            try{
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["uonline"].ToString())){
                    con.Open();

                    var query = new SqlCommand("SELECT Id, Name FROM Category WHERE Id='" + id + "'", con);

                    using (var dr = query.ExecuteReader()){

                        while (dr.Read()){

                            category = new Category();
                            category.Id = Convert.ToInt32(dr["Id"]);
                            category.Name = dr["Name"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex){
                throw;
            }
            return category;
        }

        public bool Insert(Category t)
        {
            bool rpta = false;

            try{
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["uonline"].ToString())){
                    con.Open();

                    var query = new SqlCommand("INSERT INTO Category VALUES (@Name)", con);

                    query.Parameters.AddWithValue("@Name", t.Name);

                    query.ExecuteNonQuery();

                    rpta = true;
                }
            }
            catch (Exception ex){
                throw;
            }

            return rpta;
        }

        public bool Update(Category t)
        {
            bool rpta = false;

            try{
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["uonline"].ToString())){
                    con.Open();

                    var query = new SqlCommand("UPDATE Category SET Name=@Name WHERE Id=@Id", con);

                    query.Parameters.AddWithValue("@Id", t.Id);
                    query.Parameters.AddWithValue("@Name", t.Name);

                    query.ExecuteNonQuery();

                    rpta = true;
                }
            }
            catch (Exception ex){
                throw;
            }

            return rpta;
        }
    }
}
