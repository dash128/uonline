﻿using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public interface ICourseRepository : ICrudRepository<Course>
    {
        List<Course> FindByTeacher(Teacher teacher);

        List<Course> FindByCategory(Category category);
    }
}
