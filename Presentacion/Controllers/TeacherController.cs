﻿using Business;
using Business.implementation;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Presentacion.Controllers
{

    public class TeacherController : Controller
    {
        private ITeacherService teacherService = new TeacherService();
        private ICourseService courseService = new CourseService();
        private IUnitService unitService = new UnitService();

      

        public ActionResult Index()
        {
            return View(teacherService.FindAll());
        }

        public ActionResult MyCourses()
        {
            Teacher t = new Teacher();
            t.Id = 1;
            return View(courseService.FindByTeacher(t));
        }

        public ActionResult ManageCourse(int id) {
            Course c = new Course();
            c.Id = id;
            ViewBag.CourseId = id;
            return View (unitService.FindByCourse(c));
        }

        public ActionResult AddUnit(int CourseId) {
            ViewBag.CourseId = CourseId;
            return View();
        }
        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Edit(int id)
        {
            return View(teacherService.FindById(id));
        }

        public ActionResult Delete(int id)
        {
            return View(teacherService.FindById(id));
        }

        public ActionResult Details(int id)
        {
            return View(teacherService.FindById(id));
        }

        [HttpPost]
        public ActionResult Create(Teacher teacher)
        {
            bool rptaCreate = teacherService.Insert(teacher);
            if (rptaCreate){
                return RedirectToAction("Index");
            }
            return View();
        }

        [HttpPost]
        public ActionResult Edit(Teacher teacher)
        {
            bool rptaEdit = teacherService.Update(teacher);
            if (rptaEdit) {
                return RedirectToAction("Index");
            }
            return View();
        }

        [HttpPost]
        public ActionResult Delete(Teacher teacher)
        {
            bool rptaDelete = teacherService.Delete(teacher.Id);
            if (rptaDelete){
                return RedirectToAction("Index");
            }
            return View();
        }

        [HttpPost]
        public ActionResult AddUnit(Unit unit){
            bool rptaCreate = unitService.Insert(unit);
            if (rptaCreate){
                return RedirectToAction("MyCourses");
            }
            return View();
        }
    }
}
