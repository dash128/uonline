﻿using Business;
using Business.implementation;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Presentacion.Controllers
{
    public class CategoryController : Controller
    {
        private ICategoryService categoryService = new CategoryService();

        public ActionResult Index()
        {
            return View(categoryService.FindAll());
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Edit(int id) {
            return View(categoryService.FindById(id));
        }

        [HttpPost]
        public ActionResult Create(Category category)
        {
            bool rptaCreate = categoryService.Insert(category);
            if (rptaCreate){
                return RedirectToAction("Index");
            }
            return View();
        }

        [HttpPost]
        public ActionResult Edit(Category category)
        {
            bool rptaEdit = categoryService.Update(category);
            if (rptaEdit)
            {
                return RedirectToAction("Index");
            }
            return View();
        }
    }
}