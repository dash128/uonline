﻿using Business;
using Business.implementation;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Presentacion.Controllers
{
    public class CourseController : Controller
    {
        private ICourseService courseService = new CourseService();
        private ICategoryService categoryService = new CategoryService();

        public ActionResult Index(){
            return View(courseService.FindAll());
        }

        public ActionResult Create(){
            ViewBag.category = categoryService.FindAll();
            return View();
        }

        public ActionResult Edit(int id){
            return View(courseService.FindById(id));
        }


        [HttpPost]
        public ActionResult Create(Course course)
        {
            Teacher teacher = new Teacher();
            teacher.Id = 1;
            course.TeacherCode = teacher;
            ViewBag.category = categoryService.FindAll();
            bool rptaCreate = courseService.Insert(course);
            if (rptaCreate){
                return RedirectToAction("Index");
            }
            return View();
        }
    }
}