﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
using Data.implementation;
using Entity;

namespace Business.implementation
{
    public class CourseService : ICourseService
    {
        ICourseRepository courseRepository = new CourseRepository();
        ICategoryRepository categoryRepository = new CategoryRepository();

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public List<Course> FindAll()
        {
            return courseRepository.FindAll();
        }

        public List<Course> FindByCategory(Category category)
        {
            throw new NotImplementedException();
        }

        public Course FindById(int? id)
        {
            return courseRepository.FindById(id);
        }

        public List<Course> FindByTeacher(Teacher t)
        {
            return courseRepository.FindByTeacher(t); ;
        }

        public bool Insert(Course t)
        {

            Category category = categoryRepository.FindById(t.CategoryCode.Id);
            //t.CategoryCode = category;
            return courseRepository.Insert(t);
        }

        public bool Update(Course t)
        {
            throw new NotImplementedException();
        }
    }
}
