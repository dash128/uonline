﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
using Data.implementation;
using Entity;

namespace Business.implementation
{
    public class UnitService : IUnitService
    {
        private IUnitRepository unitRepository = new UnitRepository();

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public List<Unit> FindAll()
        {
            return unitRepository.FindAll();
        }

        public List<Unit> FindByCourse(Course course)
        {
            return unitRepository.FindByCourse(course);
        }

        public Unit FindById(int? id)
        {
            throw new NotImplementedException();
        }

        public bool Insert(Unit t)
        {
            return unitRepository.Insert(t);
        }

        public bool Update(Unit t)
        {
            throw new NotImplementedException();
        }
    }
}
