﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
using Data.implementation;
using Entity;

namespace Business.implementation
{
    public class TeacherService : ITeacherService
    {
        private ITeacherRepository teacherRepository = new TeacherRepository();

        public bool Delete(int id)
        {
            return teacherRepository.Delete(id);
        }

        public List<Teacher> FindAll()
        {
            return teacherRepository.FindAll();
        }

        public Teacher FindById(int? id)
        {
            return teacherRepository.FindById(id);
        }

        public bool Insert(Teacher t)
        {
            return teacherRepository.Insert(t);
        }

        public bool Update(Teacher t)
        {
            return teacherRepository.Update(t);
        }
    }
}
