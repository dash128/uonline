﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
using Data.implementation;
using Entity;

namespace Business.implementation
{
    public class CategoryService : ICategoryService
    {
        private ICategoryRepository categoryRepository = new CategoryRepository();

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public List<Category> FindAll()
        {
            return categoryRepository.FindAll();
        }

        public Category FindById(int? id)
        {
            return categoryRepository.FindById(id);
        }

        public bool Insert(Category t)
        {
            return categoryRepository.Insert(t);
        }

        public bool Update(Category t)
        {
            return categoryRepository.Update(t);
        }
    }
}
