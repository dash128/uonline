﻿using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public interface ICourseService : ICrudService<Course>
    {
        List<Course> FindByTeacher(Teacher teacher);

        List<Course> FindByCategory(Category category);
    }
}
